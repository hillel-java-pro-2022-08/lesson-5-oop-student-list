package org.example.lesson5.oop;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StudentTest {

    @Test
    void shouldCalculateAvgMark() {
        Student student = new Student("sad", "sdaf", "sdad");

        assertEquals(0, student.getAvgMark());

        student.addMark(5);
        student.addMark(4);

        assertEquals(4.5, student.getAvgMark());
    }

}