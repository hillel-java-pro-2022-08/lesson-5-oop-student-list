package org.example.lesson5.oop;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StudentListTest {

    private final static Student STUDENT1 = new Student("Ivan", "Ivanov", "Ivanovich");
    private final static Student STUDENT2 = new Student("Petr", "Ivanov", "Ivanovich");
    private final static Student STUDENT3 = new Student("Trulala", "Ivanov", "Ivanovich");

    @Test
    void shouldAddAndGetStudents() {

        StudentList studentList = new StudentList();

        assertEquals(0, studentList.size());

        studentList.add(STUDENT1);
        studentList.add(STUDENT2);
        studentList.add(STUDENT3);

        assertEquals(3, studentList.size());

        assertEquals(STUDENT1, studentList.get(0));
        assertEquals(STUDENT2, studentList.get(1));
        assertEquals(STUDENT3, studentList.get(2));
    }


    @Test
    void shouldThrowIfStudentNotExistsOnEmpty(){
        StudentList studentList = new StudentList();

        assertThrows(IndexOutOfBoundsException.class, () -> studentList.get(5));
    }

    @Test
    void shouldThrowIfStudentNotExistsOnNonEmpty(){
        StudentList studentList = new StudentList();
        studentList.add(STUDENT1);

        assertThrows(IndexOutOfBoundsException.class, () -> studentList.get(5));
    }

}