package org.example.lesson5.oop;

import java.util.Scanner;

public class Menu {
    private final StudentList studentList;
    private final Scanner scanner;
    private final StudentsUi studentsUi;

    public Menu(StudentList studentList, Scanner scanner, StudentsUi studentsUi) {
        this.studentList = studentList;
        this.scanner = scanner;
        this.studentsUi = studentsUi;
    }

    public void run() {
        while (true) {
            showMenuItems();
            int choice = getChoice();
            if (!runMenuItem(choice)) break;
        }
    }

    private boolean runMenuItem(int choice) {
        if (choice == 1) return addStudent();
        else if (choice == 2) return showStudents();
        else if (choice == 3) return addMark();
        else if (choice == 4) return false;
        else {
            System.out.println("Incorect input");
            return true;
        }
    }


    private int getChoice() {
        System.out.print("Enter your choice: ");
        int choice = scanner.nextInt();
        scanner.nextLine();
        return choice;
    }

    private void showMenuItems() {
        System.out.println(
                "1. Add student\n" +
                        "2. Show students\n" +
                        "3. Add mark\n" +
                        "4. Exit"

        );
    }

    private boolean addMark() {
        studentsUi.show(studentList);
        int number = studentsUi.readStudentNumber();
        Student student = studentList.get(number - 1);
        int mark = studentsUi.readStudentMark();
        student.addMark(mark);
        return true;
    }

    private boolean showStudents() {
        studentsUi.show(studentList);
        return true;
    }

    private boolean addStudent() {
        Student student = studentsUi.readStudent();
        studentList.add(student);
        return true;
    }
}
