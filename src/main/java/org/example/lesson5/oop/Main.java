package org.example.lesson5.oop;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        StudentList list = new StudentList();
        Scanner scanner = new Scanner(System.in);
        StudentsUi studentsUi = new StudentsUi(scanner);
        Menu menu = new Menu(list, scanner, studentsUi);
        menu.run();
    }
}
