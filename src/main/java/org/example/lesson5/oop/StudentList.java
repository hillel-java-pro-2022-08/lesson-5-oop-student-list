package org.example.lesson5.oop;

public class StudentList {
    private Student[] students;

    public void add(Student student){
        int size = size();
        Student[] tmp = new Student[size +1];
        for (int i = 0; i < size; i++) {
            tmp[i] = students[i];
        }
        tmp[size] = student;
        students = tmp;
    }

    public Student get(int i){
        if(students == null) throw new IndexOutOfBoundsException();
        return students[i];
    }

    public int size() {
        return students == null ? 0 : students.length;
    }
}
