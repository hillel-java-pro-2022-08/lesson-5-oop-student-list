package org.example.lesson5.oop;

import java.util.Arrays;

public class Student {
    private String name;
    private String surname;
    private String patronymic;

    private int[] marks;

    public Student(String name, String surname, String patronymic) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        marks = new int[0];
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", patronymic='" + patronymic + '\'' +
                '}';
    }

    public void addMark(int mark) {
        marks = Arrays.copyOf(marks, marks.length + 1);
        marks[marks.length - 1] = mark;
    }

    public double getAvgMark() {
        if (marks.length == 0) return 0;
        int sum = 0;
        for (int i = 0; i < marks.length; i++) {
            sum += marks[i];
        }
        return (double) sum / marks.length;
    }

    public String getFullName() {
        return surname+" "+name+" "+patronymic;
    }
}
