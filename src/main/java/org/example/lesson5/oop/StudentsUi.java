package org.example.lesson5.oop;

import java.util.Scanner;

public class StudentsUi {
    private final Scanner scanner;

    public StudentsUi(Scanner scanner) {
        this.scanner = scanner;
    }

    public Student readStudent() {
        String name = readString("Enter name: ");
        String surname = readString("Enter surname: ");
        String patronymic = readString("Enter patronymic: ");
        return new Student(name, surname, patronymic);
    }

    private String readString(String message) {
        System.out.print(message);
        return scanner.nextLine();
    }

    public void show(StudentList studentList) {
        for (int i = 0; i < studentList.size(); i++) {
            Student student = studentList.get(i);
            System.out.printf("%d - %s [%.2f]\n", i + 1, student.getFullName(), student.getAvgMark());
        }
    }

    public int readStudentNumber() {
        return readInt("Enter student number:");
    }

    public int readStudentMark() {
        return readInt("Enter mark:");
    }

    private int readInt(String message) {
        System.out.println(message);
        int value = scanner.nextInt();
        scanner.nextLine();
        return value;
    }
}
